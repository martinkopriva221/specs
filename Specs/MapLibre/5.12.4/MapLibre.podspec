Pod::Spec.new do |s|
s.name = 'MapLibre'
s.version = '5.12.4'
s.license = { :type => 'Unspecified' }
s.homepage = 'https://gitlab.com/horalapp/maplibre-gl-native'
s.authors = { 'MapLibre' => '' }
s.summary = 'Open source vector map solution for iOS with full styling capabilities.'
s.platform = :ios
s.source = { :http => 'https://github.com/KoprivaMartin/Pods/releases/download/5.12.4/CustomMapLibre5.12.4.zip' }
s.ios.deployment_target = '9.0'
s.ios.vendored_frameworks = 'Mapbox.xcframework'
end
